package com.example.testapp.ui.main.adapter

import androidx.recyclerview.widget.DiffUtil
import com.example.testapp.entity.MediaItem

class MediaDiffCallback(
    private val oldList: List<MediaItem>,
    private val newList: List<MediaItem>
): DiffUtil.Callback() {


    override fun getOldListSize(): Int {
        return oldList.size
    }

    override fun getNewListSize(): Int {
        return newList.size
    }

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition].mediaId == newList[newItemPosition].mediaId
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] == newList[newItemPosition]
    }
}