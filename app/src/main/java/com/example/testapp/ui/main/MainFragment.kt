package com.example.testapp.ui.main

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.testapp.R
import com.example.testapp.entity.MediaItem
import com.example.testapp.ui.detail.DetailFragment
import com.example.testapp.ui.main.adapter.MediaItemAdapter
import com.example.testapp.ui.main.adapter.SwipeCallback

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private val mediaItemAdapter = MediaItemAdapter(::openDetails)

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        viewModel.updateList()

        val recyclerView = view?.findViewById<RecyclerView>(R.id.mediaItemRv)
        recyclerView?.layoutManager = LinearLayoutManager(requireContext())
        recyclerView?.adapter = mediaItemAdapter
        val swipeCallback = ItemTouchHelper(SwipeCallback(mediaItemAdapter, requireContext()))
        swipeCallback.attachToRecyclerView(recyclerView)
        val swipeContainer = view?.findViewById<SwipeRefreshLayout>(R.id.swipeContainer)
        swipeContainer?.isRefreshing = true
        val mainFragmentTv = view?.findViewById<TextView>(R.id.mainFragmentTv)

        swipeContainer?.setOnRefreshListener {
            viewModel.updateList()
        }

        viewModel.mediaItemList.observe(viewLifecycleOwner, Observer {
            swipeContainer?.isRefreshing = false
            mainFragmentTv?.visibility = View.GONE
            mediaItemAdapter.updateList(it)
        })

        viewModel.downloadError.observe(viewLifecycleOwner, Observer {
            swipeContainer?.isRefreshing = false
            mainFragmentTv?.setText(R.string.list_load_error)
            mainFragmentTv?.visibility = View.VISIBLE
        })
    }

    private fun openDetails(mediaItem: MediaItem)
    {
        val navController = findNavController()
        val arguments = Bundle()
        arguments.putString(DetailFragment.PARAM_TITLE, mediaItem.mediaTitleCustom)
        arguments.putString(DetailFragment.PARAM_DATE, mediaItem.mediaDate.dateString)
        arguments.putString(DetailFragment.PARAM_LINK, mediaItem.mediaUrl)
        navController.navigate(R.id.action_mainFragment_to_detailFragment, arguments)
    }

}