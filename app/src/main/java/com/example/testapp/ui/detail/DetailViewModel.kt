package com.example.testapp.ui.detail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapp.repository.MediaItemRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.io.InputStream
import java.lang.Exception

class DetailViewModel : ViewModel() {

    private val mediaItemRepository = MediaItemRepository()
    private var title: String = ""
    private var date: String = ""
    private var link: String = ""
    private val _pdfInputStream = MutableLiveData<InputStream>()
    val pdfInputStream = _pdfInputStream as LiveData<InputStream>
    private val _downLoadError = MutableLiveData<Boolean>()
    val downloadError = _downLoadError as LiveData<Boolean>

    fun setInputParams(title: String, date: String, link: String) {
        this.title = title
        this.date = date
        this.link = link

        if (link.isNotEmpty())
            downloadPdf()
        else
            _downLoadError.value = true
    }

    private fun downloadPdf() {
        viewModelScope.launch {
            try {
                val response = mediaItemRepository.getPdfMedia(link)
                delay(200)
                if (response != null)
                    _pdfInputStream.value = response.byteStream()
                else
                    _downLoadError.value = true
            } catch (e: Exception) {
                _downLoadError.value = true
            }
        }
    }

}