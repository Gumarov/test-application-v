package com.example.testapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.testapp.entity.MediaItem
import com.example.testapp.repository.MediaItemRepository
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {

    private val _mediaItemList = MutableLiveData<List<MediaItem>>()
    val mediaItemList = _mediaItemList as LiveData<List<MediaItem>>
    private val mediaItemRepository = MediaItemRepository()
    private val _downloadError = MutableLiveData<Boolean>()
    val downloadError = _downloadError as LiveData<Boolean>

    fun updateList() {

        viewModelScope.launch {
            val response = mediaItemRepository.getMedia()
            if (response != null)
                _mediaItemList.value = response.content.toList()
            else
                _downloadError.value = true
        }
    }

}