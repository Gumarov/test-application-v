package com.example.testapp.ui.detail

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import com.example.testapp.R
import com.github.barteksc.pdfviewer.PDFView
import com.github.barteksc.pdfviewer.listener.OnErrorListener
import kotlinx.coroutines.delay
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class DetailFragment : Fragment() {

    companion object {

        const val PARAM_TITLE = "title"
        const val PARAM_LINK = "link"
        const val PARAM_DATE = "date"

        fun newInstance() = DetailFragment()
    }

    private lateinit var viewModel: DetailViewModel
    private var progressBar: ProgressBar? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.detail_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(DetailViewModel::class.java)

        val pdfView = view?.findViewById<PDFView>(R.id.pdfView)
        progressBar = view?.findViewById<ProgressBar>(R.id.progressBar)
        val titleTv = view?.findViewById<TextView>(R.id.titleTv)
        val dateTv = view?.findViewById<TextView>(R.id.dateTv)

        arguments?.let {
            val title = it.getString(PARAM_TITLE, "")
            val date = it.getString(PARAM_DATE, "")
            val link = it.getString(PARAM_LINK, "")
            viewModel.setInputParams(title, date, link)

            titleTv?.text = title
            dateTv?.text = formattedDate(date)
        }

        viewModel.pdfInputStream.observe(viewLifecycleOwner, Observer {
            hideProgressBar()
            pdfView?.fromStream(it)
                ?.onError(OnErrorListener { showErrorOnPdfLoad() })
                ?.load()
        })

        viewModel.downloadError.observe(viewLifecycleOwner, Observer {
            showErrorOnPdfLoad()
            hideProgressBar()
        })

    }

    private fun showErrorOnPdfLoad() {
        Toast.makeText(requireContext(), R.string.pdf_load_error, Toast.LENGTH_SHORT).show()
    }

    private fun hideProgressBar() {
        progressBar?.visibility = View.GONE
    }

    private fun formattedDate(dateString: String): String {
        return try {
            val inputDate = SimpleDateFormat("E, dd MMM yyyy HH:mm:ss Z", Locale.ENGLISH)
            val outPutDate = SimpleDateFormat("E, dd MMM yyyy", Locale.getDefault())
            val originalDate = inputDate.parse(dateString)
            outPutDate.format(originalDate)
        } catch (e: Exception) {
            ""
        }
    }

}