package com.example.testapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.testapp.R
import com.example.testapp.entity.MediaItem

class MediaItemAdapter(private val onItemClick: (MediaItem) -> Unit): RecyclerView.Adapter<MediaItemAdapter.MediaItemVH>() {

    inner class MediaItemVH(itemView: View): RecyclerView.ViewHolder(itemView) {
        private var titleTv = itemView.findViewById<TextView>(R.id.mediaTitle)

        fun bind(mediaItem: MediaItem) {
            titleTv.text = mediaItem.mediaTitleCustom.replace("<br />\r\n", "")

            titleTv.setOnClickListener {
                onItemClick.invoke(mediaItem)
            }
        }
    }

    init {
        setHasStableIds(true)
    }

    private val itemList = mutableListOf<MediaItem>()

    override fun getItemId(position: Int): Long {
        return itemList[position].mediaId
    }

    fun updateList(newList: List<MediaItem>) {

        val diffResult = DiffUtil.calculateDiff(MediaDiffCallback(itemList, newList))
        diffResult.dispatchUpdatesTo(this)

        itemList.clear()
        itemList.addAll(newList)
    }

    fun removeItem(position: Int) {
        val newList = itemList.toMutableList()
        newList.removeAt(position)
        updateList(newList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MediaItemVH {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.li_media_item, parent, false)
        return MediaItemVH((view))
    }

    override fun onBindViewHolder(holder: MediaItemVH, position: Int) {
        holder.bind(itemList[position])
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

}