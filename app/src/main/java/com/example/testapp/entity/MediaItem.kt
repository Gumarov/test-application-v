package com.example.testapp.entity

import com.google.gson.annotations.SerializedName

data class MediaItem(
    @SerializedName("mediaId") val mediaId: Long,
    @SerializedName("mediaUrl") val mediaUrl: String,
    @SerializedName("mediaUrlBig") val mediaUrlBig: String,
    @SerializedName("mediaDate") val mediaDate: MediaDate,
    @SerializedName("mediaTitleCustom") val mediaTitleCustom: String
)
