package com.example.testapp.entity

import com.google.gson.annotations.SerializedName

data class MediaResponse(
    @SerializedName("status") val status: Boolean,
    @SerializedName("lang") val lang: String,
    @SerializedName("content") val content: Array<MediaItem>
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MediaResponse

        if (status != other.status) return false
        if (lang != other.lang) return false
        if (!content.contentEquals(other.content)) return false

        return true
    }

    override fun hashCode(): Int {
        var result = status.hashCode()
        result = 31 * result + lang.hashCode()
        result = 31 * result + content.contentHashCode()
        return result
    }
}
