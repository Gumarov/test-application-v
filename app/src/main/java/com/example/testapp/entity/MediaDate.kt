package com.example.testapp.entity

import com.google.gson.annotations.SerializedName

data class MediaDate(
    @SerializedName("dateString") val dateString: String,
    @SerializedName("year") val year: String
)
