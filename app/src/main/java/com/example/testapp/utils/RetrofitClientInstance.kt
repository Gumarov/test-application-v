package com.example.testapp.utils

import com.example.testapp.BuildConfig
import com.example.testapp.api.MediaApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitClientInstance {

    private var retrofit: Retrofit? = null
    private const val BASE_URL = "https://www.monclergroup.com/"
    private const val CONNECTION_TIMEOUT = 60L

    private val okHttpBuilder = OkHttpClient.Builder()
        .retryOnConnectionFailure(true)
        .readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
        .addInterceptor(HttpLoggingInterceptor().apply {
            level = if (BuildConfig.DEBUG) {
                HttpLoggingInterceptor.Level.BODY
            } else {
                HttpLoggingInterceptor.Level.NONE
            }
        })

    fun get(): Retrofit {
        if (retrofit == null) {
            retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpBuilder.build())
                .build()
        }
        return retrofit!!
    }

    fun mediaApi(): MediaApi {
        return get().create(MediaApi::class.java)
    }

}