package com.example.testapp.api

import com.example.testapp.entity.MediaItem
import com.example.testapp.entity.MediaResponse
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Url

interface MediaApi {

    @GET("wp-json/mobileApp/v1/getPressReleasesDocs/")
    suspend fun getMedia(): MediaResponse

    @GET
    suspend fun getPdfMedia(@Url filePath: String): ResponseBody

}