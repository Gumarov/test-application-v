package com.example.testapp.repository

import com.example.testapp.entity.MediaResponse
import com.example.testapp.utils.RetrofitClientInstance
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import java.lang.Exception

class MediaItemRepository {

    private val mediaApi = RetrofitClientInstance.mediaApi()

    suspend fun getMedia(): MediaResponse? = withContext(Dispatchers.IO) {
        try {
            mediaApi.getMedia()
        } catch (e: Exception)
        {
            return@withContext null
        }
    }

    suspend fun getPdfMedia(filePath: String): ResponseBody? = withContext(Dispatchers.IO) {
        try {
            mediaApi.getPdfMedia(filePath)
        } catch (e: Exception)
        {
            return@withContext null
        }
    }

}